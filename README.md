# Slope analysis

This script takes a dem and an investigation point as a parameter and creates a raster file indicating the required slope (meter/meter) to reach the respective pixel from the point's location. 

This allows a quick estimation which areas 
- can be drained to the investigate point (water treatment plant) by gravity
- can be served from this point (water tank) by gravity

## Setup a virtual environment

virtualenv <path>
cd <path>
source bin/activate

## Install required libraries

pip install --upgrade -r requirements

If you get an error "... _osgeo can't be imported", run:

pip uninstall gdal && pip install gdal[numpy] --no-cache

## Usage

1. Enter virtual environment (if not yet done): 

cd <path>
source bin/activate

2. run the prog:

python sa.py -h

Read the help. Minimum required is to provide -f, -c and -u.
Most likely, -dh and -sl is required to fine-tune the results.
Provide a target (output) file path via -o for the final run.

3. Read and visualize the raster, e.g. using Quantuum GIS

Example result:
![Result]("slopes_to-tank-51.759_14.366-dh+2.0m-sl-0.0015.tif")

Screenshot QGIS
![Screenshot](1.jpg)
