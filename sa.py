"""
Create a raster indicating the slopes that are required to reach a provided point
"""

from osgeo import gdal, osr, ogr
from osgeo import gdal_array
import numpy as np
import sys
import os
import argparse
import rasterio
import rasterio.mask
import geojson
from rasterio.io import MemoryFile
import math
from contextlib import contextmanager
import os, tempfile

DEBUG=False

if DEBUG:
	import matplotlib
	from rasterio.plot import show
	matplotlib.use('GTK3Agg') # plotting

gdal.UseExceptions()
DEFAULT_MAX_DIST=5000


def transform_ogr_geom(geom, t_crs, s_crs=None, debug=DEBUG):
	"""
	TODO: detect CRS from geom
	"""

	src_crs = geom.GetSpatialReference()

	# override CRS
	if not src_crs and s_crs:
		src_crs = osr.SpatialReference()
		src_crs.ImportFromEPSG(s_crs)
		point = ogr.Geometry(ogr.wkbPoint)
		geom.AssignSpatialReference(src_crs)
		if debug: print(f'Override geometry source CRS with {src_crs}')

	elif not src_crs:
		print(f"Can't determine geometry's CRS")
		return False

	tgt_crs = osr.SpatialReference()
	tgt_crs.ImportFromEPSG(t_crs)

	to_utm_transformation = osr.CoordinateTransformation(src_crs, tgt_crs)

	# transform point to UTM (in-situ, so we have to clone point first, if we also want to reference it later
	point_utm = ogr.CreateGeometryFromWkb(geom.ExportToIsoWkb()) # clone
	point_utm.Transform(to_utm_transformation)

	if debug: print(f"""Transformed/Reprojected geometry {geom} to {point_utm}""")
	return point_utm


def get_ring_geometry(geom, b_min_m, b_max_m, debug=DEBUG) -> ogr.Geometry:
	"""
	Creates a UTM Circle (b_min_m == 0) or ring geometry from given point + buffer
	"""
	# buffer point outside (circle) and inside (ring)
	inner_circle = geom.Buffer(b_min_m) if b_min_m else None
	outer_circle = geom.Buffer(b_max_m)

	# spatial difference, if ring
	ring = outer_circle.SymmetricDifference(inner_circle) if inner_circle else outer_circle
	if debug: print(f'Buffered point {geom} by max {b_max_m}m')
	return ring


def get_raster_crs(src, fallback_crs=None):
	"""
	"""
	src_crs = src.crs

	if src_crs:
		print(f'Detected CRS from rasterio Dataset: {src_crs}')

	elif fallback_crs:
		src_crs = fallback_crs
		print(f'Use manually provided CRS for raster: {src_crs}')

	else:
		print(f"Could not detect CRS from image not a CRS was provided. Use parameter ... and retry")
		sys.exit(1)

	return src_crs


@contextmanager
def reproject_raster(in_path, crs, src=None, debug=DEBUG):
	"""
	Reproject/transform a given raster image from filesystem (in_path=) or rasterio DataSet (src=) to CRS (crs=)
	"""
	if not src:
		src = rasterio.open(in_path, "r")
		if debug: print(f'rasterio {in_path} opened')

	if not src:
		print(f'provide either dst or filesystem infile')
		return False # sys.exit(1)

	src_crs = get_raster_crs(src, None)

	transform, width, height = rasterio.warp.calculate_default_transform(src_crs, crs, src.width, src.height, *src.bounds)

	kwargs = src.meta.copy()
	kwargs.update({
		'crs': crs,
		'transform': transform,
		'width': width,
		'height': height
		})

	with MemoryFile() as memfile:
		with memfile.open(**kwargs) as dst:
			for i in range(1, src.count + 1):
				rasterio.warp.reproject(
					source=rasterio.band(src, i),
					destination=rasterio.band(dst, i),
					src_transform=src.transform,
					src_crs=src.crs,
					dst_transform=transform,
					dst_crs=crs,
					resampling=rasterio.warp.Resampling.bilinear)

		with memfile.open() as dataset:  # Reopen as DatasetReader
			yield dataset  # 'yield' as we're a contextmanager

	if debug: print(f"""Unknown error reprojecting {in_path}""")
	return False


@contextmanager
def mask_raster(in_path=None, src=None, geom=None, crs=None, debug=DEBUG):
	"""
	Masks a raster to the extent of the provided geometry
	"""
	if not src:
		src = rasterio.open(in_path, "r")
		if debug: print(f'rasterio {in_path} opened')

	if not src:
		print(f'provide either dst or filesystem infile')
		return False # sys.exit(1)

	src_crs = get_raster_crs(src, None)

	# FIXME: warn, if geom CRS != image CRS

	out_image, out_transform = rasterio.mask.mask(src, geom, crop=True)
	out_meta = src.meta.copy()
	out_meta.update({ # "driver": "GTiff",
		"height": out_image.shape[1],
		"width": out_image.shape[2],
		"transform": out_transform
		})

	with MemoryFile() as mf:
		with mf.open(**out_meta) as dst:
			dst.write(out_image)

		with mf.open() as dataset:
			yield dataset

	if debug: print(f"""Unknown error masking {in_path}""")
	return False


@contextmanager
def slope_analysis(in_path=None, src=None, point_tuple=None, delta_height=None, slope_limit=None, debug=DEBUG, clip_to_slope=None):
	# reproject raster to project crs

	if not src:
		src = rasterio.open(in_path, "r")

	if not src:
		print(f'provide either dst or filesystem infile')
		# TODO: implement memory file
		sys.exit(1)

	# clip_to_slope = eval("1/500")

	src_nodata = src.nodata
	target_nodata = 0
	print(f"SRC nodata {src_nodata} -> TGT nodata {target_nodata}")

	# get input numpy array
	s_arr = src.read(1)

	x = point_tuple[0] # 302337.246438635
	y = point_tuple[1] # 5963445.307904
	a, b = src.index(x, y)
	z = s_arr[a,b]
	print(f'Indestigation point is {x}/{y} with a height of {z}m at raster index {a}/{b}.')

	if delta_height:
		z = z + delta_height
		print(f'Added an offset of {delta_height}m to the height, so {z}m will be used.')

	# get output numpy array:
	size1, size2 = s_arr.shape
	proximity = np.zeros(shape=(size1, size2), dtype=float)

	slope_limit = slope_limit if slope_limit and slope_limit != 0 else None
	if slope_limit:
		if slope_limit > 0:
			print(f"A positive slope limit is desired, this is typical to investigate a WTP location")

		elif slope_limit < 0:
			print(f"A negative slope limit is desired, this is typical when investigating a tank location")

	for (i, j), val in np.ndenumerate(s_arr):
		slope = target_nodata

		# skip nodata
		if not s_arr[i,j] == src_nodata:

			if (i, j) == (a, b):
				# mark the point location itself!
				if slope_limit:
					if slope_limit > 0:
						slope = 1

					elif slope_limit < 0:
						slope = -1

				else:
					slope=target_nodata

			else:
				px_x, px_y = src.xy(i, j)

				""" the magic begins """

				dist = math.sqrt(math.pow((px_x-x),2)+math.pow((px_y-y),2)) # m, because of UTM
				dem_pt_height = s_arr[i,j] # dem value

				slope = (dem_pt_height-z)/dist

				# if slope < valid_range[0] and slope > valid_range[1]:
				if slope_limit and ((slope_limit > 0 and slope < slope_limit) or (slope_limit < 0 and slope > slope_limit)):
					slope = target_nodata

				if debug: print(f'{i}/{size1} ... {j}/{size2} = {dem_pt_height} m height @ {dist}m to ({z}) = {slope}')

		proximity[i,j] = slope # img_as_array[i,j]

	out_meta = src.meta.copy()
	out_meta.update(
		nodata=target_nodata,
		dtype=rasterio.float32,
	)

	with MemoryFile() as mf:
		with mf.open(**out_meta) as dst:
			dst.ndata = target_nodata
			dst.write(proximity, 1)

		with mf.open() as dataset:
			yield dataset


def main(dem_file_path, point_tuple, t_epsg, point_s_epsg=4326
	, point_delta_height=0, dem_s_epsg:int=None, band_num=1
	, max_dist_m=DEFAULT_MAX_DIST, debug=DEBUG
	, slope_limit=None, outfile=None):
	"""
	"""

	# create a names temporary file, if no output is set
	if not outfile:
		tmp = tempfile.NamedTemporaryFile(suffix='.tif'
			, prefix=os.path.basename(__file__)
		)
		tmp_dir = os.path.dirname(tmp.name)

		# build basename from params
		basename = '-'.join(list(filter(None,[
			'slopes_to'
			, 'wtp' if slope_limit and slope_limit > 0 else 'tank' if slope_limit and slope_limit < 0 else None
			, str(point_tuple).replace('(','').replace(')','').replace(',','').replace(' ','_')
			, f'dh{point_delta_height if point_delta_height<0 else f"+{point_delta_height}"}m' if point_delta_height else 'dh_0'
			, f'sl{slope_limit if slope_limit<0 else f"+{slope_limit}"}' if slope_limit else None
			])))

		outfile = os.path.join(tmp_dir, basename + '.tif')
		# print(outile)

	t_crs = osr.SpatialReference()
	t_crs.ImportFromEPSG(t_epsg)
	t_epsg_string=f'EPSG:{t_epsg}'
	if debug: print(f'Target CRS: {t_epsg_string}')

	""" investigated point """
	src_crs = osr.SpatialReference()
	src_crs.ImportFromEPSG(point_s_epsg)

	point = ogr.Geometry(ogr.wkbPoint)
	point.AssignSpatialReference(src_crs)
	point.AddPoint(point_tuple[0], point_tuple[1])

	# geom = transform_ogr_geom(geom=point, t_crs=t_epsg, s_crs=point_s_epsg, debug=debug)
	geom = transform_ogr_geom(geom=point, t_crs=t_epsg, debug=debug)

	ring_geom = get_ring_geometry(geom, 0, max_dist_m, debug=debug) # , point_s_epsg) # , t_crs)
	print(f"""Investigated investigation area's BBox is: {ring_geom.GetEnvelope()}""")

	with reproject_raster(dem_file_path, t_epsg_string) as utm_raster:
		print(f"""Reprojected raster to {t_epsg_string}""")

		with mask_raster(in_path=None, src=utm_raster, geom=[geojson.loads(ring_geom.ExportToJson())], debug=debug) as masked_raster:
			print(f"""Masked DEM by investigated radius to a w/h {masked_raster.height}x{masked_raster.height}px image""")

			investigation_point_tuple = (geom.GetX(), geom.GetY())
			with slope_analysis(in_path=None, src=masked_raster, point_tuple=investigation_point_tuple
				, delta_height=point_delta_height, slope_limit=slope_limit, debug=debug) as sa:

				if debug:
					raster = sa
					rasterio.plot.show(raster, cmap="BrBG")

				metadata = sa.meta.copy()
				metadata.update(
					# dtype=rasterio.float32,
					# count=1,
					compress='lzw')

				with rasterio.open(outfile, "w", **metadata) as fs_data:
					fs_data.write(sa.read())
					print(f'Image written to "{outfile}"')
					sys.exit(0)

	print(f"""Unknown error in main""")
	sys.exit(1)


if __name__ == '__main__':
	parser = argparse.ArgumentParser(#prog = 'ProgramName',
		description = """Given a digital elevation model (DEM) and a coordinate, this programm will create a raster with the required slopes to reach thi point.
			That might be served by gravity.
			Simplified procedure, beware of pitfalls. But this allows to estimate the drainable area of a water treatment plant (all areas with pixels of sufficient high, positive slopes) or to estimate the potential service area of a water tank (negative slopes).
			A NODATA value of "0" is set for the resulting DEM
			TODO: Clip DEM to "WTP" ot "tank" values""",
		epilog = 'Written for Justus @ Gauff Consultants Africa. Q & A: tobias.b.wolff@gmx.de')

	""" Command line args """

	parser.add_argument('--dem_file_path', '-f', help="Path to input DEM", type=str)
	parser.add_argument('--point_coordinate_tuple', '-p', nargs=2, help="Point x/y coordinates, separated by space: e.g. '12.434 -1.234'", type=float)
	parser.add_argument('--utm_crs', '-u', help="All calculations are done in UTM. Provide a EPSG numer of the target DEM", type=int)
	parser.add_argument('--point_crs', '-ep', help="CRS as the EPSG number of point tuple (-p). TODO: DEFAULTS TO 4326 of lower then 360/360, else target utm", type=int, default=4326)
	parser.add_argument('--max_dist_m', '-m', help="Maximum distance to investigate around the point in m. Defaults to {DEFAULT_MAX_DIST}", type=float, default=DEFAULT_MAX_DIST)
	parser.add_argument('--delta_height', '-dh', help="Point offset from ground. E.g. provide '15' if the investigated point is a 15 m high water tower. Defaults to 0", type=float, default=0)
	parser.add_argument('--dem_crs', '-ed', help="Override the EPSG of input DEM (typically when if it can't be detected from given image)", type=int, default=None)

	parser.add_argument('--slope_limit', '-sl', help="limit results to greater (when positive, e.g. for WTP) or less (nagative values, e.g. for water tanks) then this value", type=float, default=None)
	parser.add_argument('--outfile', '-o', help="Outfile path. Default is determined from infile name", type=str, default=None)

	# debug
	parser.add_argument('--debug', '-d', help="Debug outputs. Also plots a preview if 'matplotlib' are installed", type=bool, default=DEBUG)

	""" parse args """

	args = parser.parse_args()
	dem_file_path = args.dem_file_path
	point_coordinate_tuple = tuple(args.point_coordinate_tuple)
	utm_crs = args.utm_crs
	delta_height = args.delta_height
	debug = args.debug
	dem_crs = args.dem_crs
	max_dist_m = args.max_dist_m
	slope_limit=args.slope_limit
	outfile=args.outfile

	""" call main """

	sys.exit(main(point_tuple=point_coordinate_tuple, dem_file_path=dem_file_path, max_dist_m=max_dist_m
		, t_epsg=utm_crs, dem_s_epsg=dem_crs, band_num=1, point_delta_height=delta_height, debug=debug
		, slope_limit=slope_limit, outfile=outfile))
